<?php

namespace Arthurfaria\Greeter;

class Greeter
{
    public function giveHello(String $sName)
    {
        return 'Hello ' . $sName . '! How are you doing today?';
    }
}