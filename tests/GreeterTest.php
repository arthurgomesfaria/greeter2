<?php

namespace Tests;

use PHPUnit\Framework\TestCase;
use Arthurfaria\Greeter\Greeter;

class GreeterTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testGreeter()
    {
        $greeter = new Greeter();
        $sName = "SEBRAE";
        $this->assertTrue('Hi ' . $sName . '! How are you doing today?' == $greeter->greet($sName));
    }
}